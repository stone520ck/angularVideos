angular.module('starter', ['ui.router','infinite-scroll', 'ngAnimate','ngCookies','starter.controllers','starter.services'])

    .constant('loading', true)

    .config(function ($stateProvider, $urlRouterProvider,$httpProvider) {
        delete $httpProvider.defaults.headers.common['X-Requested-With'];

        $urlRouterProvider.otherwise("/home");
        //
        // Now set up the states
        $stateProvider
            .state('home/:id', {
                url: "/home",
                templateUrl: "partials/home.htm",
                controller: 'HomeCtrl'
            })
            .state('users', {
                url: "/users",
                templateUrl: "partials/users.htm",
                controller: 'UsersCtrl'
            })
            .state('top', {
                url: "/top/:type",
                templateUrl: "partials/top.htm",
                controller: 'TopCtrl'
            })



    })

