angular.module('starter.services', [])
    .factory('getData', function($http,$location,Static) {


        var GetData = function(params) {
            this.items = [];
            this.busy = false;
            this.page  = 1;
            this.pagecount = 10;
            this.end = false;
            this.error = false;
            this.params = params ;



        };

        GetData.prototype._getStarts = function(num){
            var num = num.replace(/,/g,"");
            var num = num*1;
            var s = 0;
            if(num>5000)
                s = 1;
            if(num>10000)
                s = 2;
            if(num>200000)
                s=3;
            if(num>500000)
                s=4;
            if(num>1000000)
                s=5;
            //console.log(s,num);
            return s;
        }

        GetData.prototype.nextPage = function() {

            if (this.busy ) return;

            this.busy = true;
            var url = Main.sprintf(this.params.url,this.page);
            $http.get(url).success(function(data) {

                var items = data.data;
                console.log(url,items);
                for (var i = 0; i < items.length; i++) {
                    if(items[i].click_count)
                        items[i].star = this._getStarts(items[i].click_count);
                    this.items.push(items[i]);
                }

                this.page++;
                this.busy = false;




            }.bind(this))
                .error(function(data, status, headers, config) {
                    this.error = true;
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            }.bind(this));
            ;
        };

        return GetData;
    })
.factory('NAV',function(){
    return [


    ]
}).factory('Static',function($location){
    //var host = $location.host() || 'testms.tunnel.mobi';
    var host = 'http://101.200.130.104/DotaVideoApi/';
   return {
       host:host
   }
 })