angular.module('starter.services', [])

/**
 * A simple example service that returns some data.
 */
    .factory('Main',function($location,$indexedDB,tablename){
        DB = $indexedDB.objectStore(tablename);

        refresh = function(obj,callBack){
            var callBack = callBack || null;
            $(function(){
                $(obj).pullToRefresh({refresh:300,resetSpeed:'1000ms'})
                    .on("start.pulltorefresh", function (evt, y){
                        //$state.html('Start!! ' + evt + ', '+y)
                    })

                    .on("move.pulltorefresh", function (evt, percentage){
                       // $state.html('Move.. ' + evt + ', '+percentage)
                    })

                    .on("end.pulltorefresh", function (evt){
                        //$state.html('End.. ' + evt)
                        // $(this).html("Pull-me please");
                    })

                    .on("refresh.pulltorefresh", function (evt, y){
                        console.log('refresh.pulltorefresh'+y);
                        if(callBack)
                            callBack(getDay(1));
                        // $(this).html('Refresh.. ' + evt + ', '+y)
                    });


            })
        }
        loadingShow = function () {
            return true;
            console.log('loadingShow');

        };
        loadingHide = function () {
            return false;
            console.log('loadingHide');

        };

        datePicker = (new Date()).toLocaleDateString()


        userAgent = navigator.userAgent;



        redirect = function (url) {
            var url = url || '/home';
            $location.path(url);
        }
        return {
            DB:DB,
            loadingShow:loadingShow,
            loadingHide:loadingHide,
            datePicker:datePicker,
            userAgent:userAgent,
            redirect:redirect,
            refresh:refresh

        }
    })
    .factory('Orders', function ($http, $q) {
        // Might use a resource here that returns a JSON array

        // Some fake testing data
        var orders = function (time) {
            var time = time || null;
            var deferred = $q.defer();
            var host = 'http://ethnicharms.com';
           // var host = 'http://man0sions/man0sionsShop';
            $http.get(host+'/site/orderApi.html?root=1&passwd=9B2FA194EF5A0EB7&time='+JSON.stringify(time))
                .success(function (data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function(data, status, headers, config){
                    //deferred.resolve(data);
                    deferred.reject('http error');
                })
            return deferred.promise;
        }

        return {
            orders: orders
        }
    })
    .factory('isMobile',function(){
        var isMobile = {
            Android: function() {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function() {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function() {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };
        return isMobile;
    })
