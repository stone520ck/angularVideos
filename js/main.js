function Main(){
}
Main.prototype.setCookie = function(name, value, expires, path) {

    value = escape(value);
    expires = expires ? expires: '';

    if (expires == '') {
        date = new Date();
        date.setDate(date.getDate() + 7);
        expires = date.toGMTString();

    }
    document.cookie = name + "=" + escape(value) + ";expires=" + expires + ";path=" + path;
}

Main.prototype.getCookie = function(c_name) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) c_end = document.cookie.length;
            return document.cookie.substring(c_start, c_end);
        }
    }
    return "";
}
Main.prototype.sprintf = function ()
{
    var arg = arguments,
        str = arg[0] || '',
        i, n;
    for (i = 1, n = arg.length; i < n; i++) {
        if(arg[i]!=null)
            str = str.replace(/%s/, arg[i]);
    }
    return str;
}
Main = new Main();
Array.prototype.unique = function(){
    var res = [];
    var json = {};
    for(var i = 0; i < this.length; i++){
        if(!json[this[i]]){
            res.push(this[i]);
            json[this[i]] = 1;
        }
    }
    return res;
}





