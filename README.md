
# angularjs 实现 dotaVideo api 调用
> 使用bootstrap，angularjs 简单实现DotaVideoApi 调用

### 查看后台api [http://git.oschina.net/man0sions/DotaVideoApi](http://git.oschina.net/man0sions/DotaVideoApi)

### 查看react native 实现 及源码下载  
[http://git.oschina.net/man0sions/DotaVideo](http://git.oschina.net/man0sions/DotaVideo)

### 项目演示地址
[http://101.200.130.104/test/angularVideos
](http://101.200.130.104/test/angularVideos)

### 效果图
![image](http://101.200.130.104/images/YH/angularVideos2.jpg)

![image](http://101.200.130.104/images/YH/angularVideos3.jpg)
